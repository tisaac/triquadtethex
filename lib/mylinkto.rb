
include Nanoc::Helpers::LinkTo

def li_to(text, target, attributes={})
  # Find path
  if target.is_a?(String)
    path = target
  else
    path = target.path
    raise RuntimeError, "Cannot create a link to #{target.inspect} because this target is not outputted (its routing rule returns nil)" if path.nil?
  end

  # Join attributes
  attributes = attributes.inject('') do |memo, (key, value)|
    memo + key.to_s + '="' + h(value) + '" '
  end

  # Create link
  "<li><a #{attributes}href=\"#{h path}\">#{text}</a></li>"
end
def li_to_unless_current(text, target, attributes={})
  # Find path
  path = target.is_a?(String) ? target : target.path

  if @item_rep && @item_rep.path == path
    # Create message
    "<li class=\"active\"><a>#{text}</a></li>"
  else
    li_to(text, target, attributes)
  end
end
