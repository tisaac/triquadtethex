
The world is a heap in our hands.
(Which, we all know, but still, perspective:
Already I can't hold my daughter in one hand.)
We all know, but let's go through this lemma,
Like the homework I used to .tex up so carefully.

\begin{enumerate}
  \item
    We're a network.  If we weren't all Fishers of Men
    before, we are now.  I always pictured Galileans
    mightily casting over the gunwales,
    but think of it instead like we're all
    floating on the surface.  We throw our nets about us,
    and catch ourselves in the process.

  \item
    Since we're all connected,
    there's a spanning tree.  We've all seen the visualizations:
    it even looks kind of like a Joshua tree.  But let's not
    mix our metaphors: we're all still floating on the surface,
    in our nets, only most of the ties are broken now.

  \item
    The nice thing about trees in math is you can put down roots anywhere.
    You are the root: it's like something just grabbed you there,
    floating on the surface, and pulled, and we all followed.

    (Here's the thing about "trees," though.  The root is the origin,
    where it all starts, and the page starts at the top, so trees in
    our diagrams mostly grow down.  But real trees grow up
    with the root at the bottom.  So when I said that something
    grabbed you from the surface and pulled,
    did you picture a life-line lifting you up out of the water?)

  \item
    Is it balanced around you?  The proof is left etc.
\end{enumerate}

So the world is a heap in your hands.
(And here again, up-versus-down comes into play.
If this were real life, with the root at the bottom, then
heap insertion would look kind of merry, like Plinko.  But
the root was at the top and it looked---slumped,
slumping---like a heap.  Nothing really rises to the top of a heap:
everything else sloughs away.)
And that O(log N), man, that's nothing. Log(300 million)?  Just twenty.
Log(7 billion)?  Twenty-three.  And so you didn't want to wake up to
Dallas, Philando, Alton, Orlando, Michael Brown, Newtown, BaghdadBrusselsSanBernardino or Paris,
but that's heap insertion.

(Of course, so is
My baby, your baby, his baby, her baby, their dog, these waffles, and congrats on your new job!
Plink, plink, plink.)

And Nate Silver will map it all out and reduce it, and
we have to admit, it's getting better.
Which, we all know.

